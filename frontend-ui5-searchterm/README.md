## Overview

This is a simple SAPUI5 frontend to read the `Searchterms` API.

## Prerequisites

- SAP BTP, Kyma runtime instance
- [Docker](https://www.docker.com/)
- [Node.js](https://nodejs.org/en/)
- [UI5 Tooling](https://sap.github.io/ui5-tooling/)
- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) configured to use the `KUBECONFIG` file downloaded from the Kyma runtime.

## Steps

### Run the frontend locally

1. Clone the project.

2. Inside the directory, run:

   ```
   npm install
   ```

3. Adjust the value of the **API_URL** parameter in the `webapp/config.js` file to match your `searchterms` API URL.

4. To start the application, run:

   ```
   npm run-script start
   ```

 The application loads at `http://localhost:8080`.

### Build the Docker image

1. Build and push the image to your Docker repository:

   ```
   docker build -t {your-docker-account}/fe-ui5-searchterm -f docker/Dockerfile .
   docker push {your-docker-account}/fe-ui5-searchterm
   ```

2. To run the image locally, adjust the value of the **API_URL** parameter in the `webapp/config.js` file and mount it into the image:

   ```
   docker run --mount type=bind,source=$(pwd)/webapp/config.json,target=/usr/share/nginx/html/config.json -p 8080:80 -d {your-docker-account}/fe-ui5-searchterm:latest
   ```

### Deploy the frontend

1. Create a new `commercesearch` Namespace:

   ```shell script
   kubectl create namespace commercesearch
   ```

2. Within the project open the file `k8s/configmap.yaml` and adjust the `API_URL` by replacing `<cluster-domain>` to the match the Kyma runtime cluster domain.  
   In `k8s/deployment.yaml` change `<account-name>` to your account name or to *maihiro* to use the default image.


3. Apply the Resources:

   ```shell script
   kubectl -n dev apply -f ./k8s/configmap.yaml
   kubectl -n dev apply -f ./k8s/deployment.yaml
   kubectl -n dev apply -f ./k8s/apirule.yaml
   ```

4. Use the APIRule to open the application:
   ```
   https://fe-ui5-searchterm.{cluster-domain}
   ```
