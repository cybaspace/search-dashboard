sap.ui.define(
  [
    "sap/ui/core/mvc/Controller",
    "../model/formatter",
    "../model/models",
    "../api/searchterms",
    "sap/ui/core/Fragment",
    "sap/ui/model/json/JSONModel",
  ],
  function (Controller, formatter, models, API, Fragment, JSONModel) {
    "use strict";

    var ordModels;

    return Controller.extend("searchterm.app.controller.Searchterms", {
      formatter: formatter,

      onInit: async function () {
        this.setViewModel();
        this.setDataModel();
      },

      setDataModel: async function () {
        const srchtrmModels = models.createSearchtermsModel();
        const searchterms = await API.getSearchterms();
        await srchtrmModels.setData(searchterms);
        this.getView().setModel(srchtrmModels, "searchterms");
        this.toggleBusy();
      },

      setViewModel: function () {
        const viewModel = new JSONModel({
          errorMsgTxt: "",
          isMsgStripVisible: false,
          isBusy: true,
        });
        this.getView().setModel(viewModel, "viewModel");
      },

      onAddSearchterm: async function () {
        const resourceBundle = this.getView().getModel("i18n").getResourceBundle();
        const title = resourceBundle.getText("addSearchterm");
        const emptySearchtermModel = API.getEmptySearchterm();

        await this.getFormDialog();
        this._oFormDialog.setModel(emptySearchtermModel);
        this._oFormDialog.bindElement("/");
        this._oFormDialog.setTitle(title);
        Fragment.byId("addEditFormFrag", "searchtermTxt").setEditable(true);

        this._oFormDialog.open();
      },

      onShowEdit: async function (oEvent) {
        const stModel = this.getView().getModel("searchterms");
        const resourceBundle = this.getView().getModel("i18n").getResourceBundle();
        const title = resourceBundle.getText("editSearchterm");
        const searchtermPath = oEvent.getSource().getBindingContext("searchterms").getPath();
        const tempStModel = API.getEmptySearchterm();

        tempStModel.setData(stModel.getProperty(searchtermPath));

        await this.getFormDialog();
        this._oFormDialog.setModel(tempStModel);
        this._oFormDialog.bindElement(searchtermPath);
        this._oFormDialog.setTitle(title);
        Fragment.byId("addEditFormFrag", "searchtermTxt").setEditable(false);

        this._oFormDialog.open();
      },

      saveSearchtermForm: async function (oEvent) {
        const searchtermPath = oEvent.getSource().getBindingContext().getPath();
        const data = oEvent.getSource().getModel().getProperty("/");
        const isAddSearchterm = Fragment.byId("addEditFormFrag", "searchtermTxt").getEditable();

        if (isAddSearchterm) {
          await this.addSearchterm(data);
        } else {
          await this.editSearchterm(data, searchtermPath);
        }
        this._oFormDialog.close();
      },

      closeDialog: function (oEvent) {
        const dialog = oEvent.getSource().getParent();
        dialog.close();
      },

      toggleBusy: function () {
        const viewModel = this.getView().getModel("viewModel");
        const isBusy = viewModel.getProperty("/isBusy");
        viewModel.setProperty("/isBusy", !isBusy);
      },

      addSearchterm: async function (data) {
        this.toggleBusy();
        try {
          const response = await API.addSearchterm(data);
          const stModel = this.getView().getModel("searchterms");
          const position = stModel.getData().length;
          stModel.setProperty("/" + position, response[0]);
        } catch (err) {
          this.showErrorMsq(err);
          console.log(err);
        }
        this.toggleBusy();
      },

      showErrorMsq: function (err) {
        const viewModel = this.getView().getModel("viewModel");
        viewModel.setProperty("/errorMsgTxt", err);
        viewModel.setProperty("/isMsgStripVisible", true);
      },

      getFormDialog: async function () {
        if (!this._oFormDialog) {
          this._oFormDialog = await Fragment.load({
            id: "addEditFormFrag",
            name: "searchterm.app.view.SearchtermForm",
            controller: this,
          });
          this.getView().addDependent(this._oFormDialog);
        }
        return this._oFormDialog;
      },

    });
  }
);
