## Overview

This provides a Serverless Function configured as an API endpoint for communication with the MS SQL database in the `database-mssql` folder. The `deployment.yaml` defines the Function definition as well as an APIRule to expose the Function without authentication. The Deployment contains the following parameters for the `database-mssql` example that you can configure to modify the default options:

| Parameter     | Value                                  |
| -------- | -------------------------------------- |
| **database** | `DemoDB`                                 |
| **host**     | `searchterm-db.commercesearch.svc.<cluster-domain>` |
| **password** | `Surprise666`                               |
| **username** | `sa`                                     |

## Prerequisites

- SAP BTP, Kyma runtime instance
- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) configured to use the `KUBECONFIG` file downloaded from the Kyma runtime

## Steps

### Deploy the Function

1. Create a new `commercesearch` Namespace:

   ```shell script
   kubectl create namespace commercesearch
   ```

2. Apply the Deployment:  
   If you want to use another namespace than `commercesearch` change it in the file `k8/deployment.yaml`
   ```   
   - name: host
     value: searchterm-db.commercesearch.svc.cluster.local
   ```
   Execute
   ```shell script
   kubectl -n dev apply -f ./k8s/deployment.yaml
   ```

3. Verify that the Function is up and running:

   ```shell script
   kubectl -n dev get function api-mssql-function
   ```

4. Use the APIRule:
   - `https://api-searchterm.{cluster-domain}/searchterm`
   - `https://api-searchterm.{cluster-domain}/searchterm/{searchterm}` (e.g. *searchterm* = t-shirt)
