const sql = require('mssql')

const config = {
    user: process.env.username,
    password: process.env.password,
    server: process.env.host,
    database: process.env.database,
}

module.exports = {
    main: async function (event, context) {
        try {
            const method = event.extensions.request.method
            const searchterm = event.extensions.request.path.split("/")[2];
            const pool = await sql.connect(config)
            const request = new sql.Request()

            switch (method) {
                case "GET":
                    if (searchterm) {
                        return await getSearchterm(request, searchterm);
                    } else {
                        return await getSearchterms(request);
                    }
                case "POST":
                    return await addSearchterm(request, event.data, event["ce-type"]);
                default:
                    event.extensions.response.status(500).json({
                        "message": "Unhandled method was received", "error": "Unhandled method was received"
                    });
            }
        } catch (err) {
            // ... error checks
            console.log("ERROR catch: ", err);
            event.extensions.response.status(500).json({
                "message": "An error occurred during execution", "error": err
            });
        }

        sql.on('error',
            err => {
                // ... error handler
                console.log("ERROR handler: ", error);
                event.extensions.response.status(500).json({
                    "message": "Connection to the database could not be established", "error": err
                });
            })
    }
}

async function getSearchterms(request) {
    try {
        let result = await request.query('select * from Searchterms order by created desc')
        return result.recordsets[0];
    } catch (err) {
        throw err;

    }
}

async function getSearchterm(request, searchterm) {
    try {
        let result = await request.query(`select * from Searchterms where searchterm = '${searchterm}' order by created desc`)
        return result.recordsets[0];
    } catch (err) {
        throw err;
    }
}

async function addSearchterm(request, data, eventtype) {
    console.log("received event: "+eventtype)
    switch (eventtype) {
        case "tracking.search":
            return addNewSearchterm(request, data, 0)
        case "tracking.searchnoresults":
            return addNewSearchterm(request, data, 1)
        case "tracking.refinesearch":
            return updateSearchterm(request, data, 'refined')
        case "tracking.pagethroughsearchresults":
            return updateSearchterm(request, data, 'pagechanged')
        default:
            throw {
                "name": "UnsupportedEventtypeException",
                "message": "Eventtype "+eventtype+" is not supported"
            }
    }
}

async function addNewSearchterm(request, data, noresult) {
    console.log("add searchterm "+data.searchterm+", with noresult: "+noresult)
    try {
        let result = await request.query(
            `insert into Searchterms (searchterm, basesiteid, noresult, userid) values ('${data.searchterm}', '${data.baseSiteUid}', ${noresult}, '${data.userId}'); select * from Searchterms where searchterm = '${data.searchterm}'`);
        return result.recordsets[0];
    } catch (err) {
        throw err
    }
}

async function updateSearchterm(request, data, column) {
    console.log("update searchterm for user "+data.userId+" on column "+column)
    try {
        let result = await request.query(
            `update top (1) Searchterms set ${column}=${column}+1 where userid='${data.userId}' order by created; select * from Searchterms where userid='${data.userId}' order by created`);
        return result.recordsets[0];
    } catch (err) {
        throw err
    }
}