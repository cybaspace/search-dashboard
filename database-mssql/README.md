## Overview

MS SQL database configured with to use as `SearchtermDB` database which contains one `Searchterms` table populated with sample data of commerce searches. The `app/setup.sql` file handles the generation of the database, table, and data. Within the `app/init-db.sh` file, you can also configure the database user and password. They must match the configuration of the Secret defined within the `k8s/deployment.yaml` file.

## Prerequisites

- SAP BTP, Kyma runtime instance
- [Docker](https://www.docker.com/)
- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) configured to use the `KUBECONFIG` file downloaded from the Kyma runtime.

## Deploy the database

1. Create a new `commercesearch` Namespace:

   ```shell script
   kubectl create namespace commercesearch
   ```

2. Build and push the image to your Docker repository:  
   This step is optional - you can use *maihiro* instead of an own docker account.

   ```shell script
   docker build -t {your-docker-account}/searchterm-db -f docker/Dockerfile .
   docker push {your-docker-account}/searchterm-db
   ```

3. Apply the PersistentVolumeClaim:

   ```shell script
   kubectl -n commercesearch apply -f ./k8s/pvc.yaml
   ```

4. Apply the Secret:

   ```shell script
   kubectl -n commercesearch apply -f ./k8s/secret.yaml
   ```

5. Apply the Deployment:  
   Change `<account-name>` in `k8s/deployment.yaml` to *your-docker-account*.  
   ```shell script
   kubectl -n commercesearch apply -f ./k8s/deployment.yaml
   ```

6. Verify that the Pod is up and running:

   ```shell script
   kubectl -n commercesearch get po
   ```

   The expected result shows that the Pod for the `mssql` Deployment is running:

   ```shell script
   NAME                                     READY   STATUS    RESTARTS   AGE
   searchterm-db-6df65c689d-nf9dk           2/2     Running   0          93s
   ```

## Run the Docker image locally

To run the Docker image locally, run this command:

```shell script
docker run -e ACCEPT_EULA=Y -e SA_PASSWORD=Surprise666 -p 1433:1433 -d {docker id}/searchterm-db
```
