CREATE DATABASE CommerceSearchDB;
GO
USE CommerceSearchDB;
GO
CREATE TABLE SearchTerms
(
    searchterm nvarchar(60) NOT NULL,
    basesiteid nvarchar(50),
    noresult bit,
    pagechange smallint DEFAULT 0,
    refined smallint DEFAULT 0,
    userid nvarchar(50) DEFAULT 'XXXXXXXX-XXXX-XXXX-XXXXXXXXXX',
    created DATETIME DEFAULT(getdate())
);
GO
INSERT INTO Searchterms
    (searchterm, basesiteid, noresult, created)
VALUES
    ('sony', 'electronics-spa', 0, '2020-10-01T04:32:03'),
    ('cameras', 'electronics-spa', 0, '2020-10-02T04:32:03'),
    ('slr', 'electronics-spa', 0, '2020-10-04T04:32:03'),
    ('webcam', 'electronics-spa', 0, '2020-10-15T04:32:03'),
    ('cameras', 'electronics-spa', 0, '2020-11-01T04:32:03'),
    ('webcast', 'electronics-spa', 1, '2020-11-15T04:32:03'),
    ('sony', 'electronics-spa', 0, '2020-11-16T04:32:03'),
    ('webcam', 'electronics-spa', 0, '2020-12-01T04:32:03'),
    ('sonx', 'electronics-spa', 1, '2020-12-07T04:32:03'),
    ('cameras', 'electronics-spa', 0, '2020-12-13T04:32:03'),
    ('digital', 'electronics-spa', 0, '2021-01-01T04:32:03'),
    ('cameras', 'electronics-spa', 0, '2021-01-05T04:32:03'),
    ('film', 'electronics-spa', 0, '2021-01-12T04:32:03'),
    ('digital', 'electronics-spa', 0, '2021-01-17T04:32:03'),
    ('webcam', 'electronics-spa', 0, '2021-01-20T04:32:03'),
    ('cameras', 'electronics-spa', 0, '2021-01-21T04:32:03'),
    ('slr', 'electronics-spa', 0, '2020-10-25T04:32:03'),
    ('rayban', 'apparel-de', 0, '2020-10-01T06:32:03'),
    ('t-shirt', 'apparel-de', 0, '2020-10-02T06:32:03'),
    ('sunglas', 'apparel-de', 0, '2020-10-04T06:32:03'),
    ('t-shirt', 'apparel-de', 0, '2020-10-15T06:32:03'),
    ('sunglas', 'apparel-de', 0, '2020-10-20T06:32:03'),
    ('shoes', 'apparel-de', 0, '2020-11-01T06:32:03'),
    ('ray-ban', 'apparel-de', 0, '2020-11-05T06:32:03'),
    ('route69', 'apparel-de', 1, '2020-11-20T06:32:03'),
    ('orange', 'apparel-de', 0, '2020-12-01T06:32:03'),
    ('orange', 'apparel-de', 0, '2020-12-09T06:32:03'),
    ('ray-ban', 'apparel-de', 0, '2020-12-15T06:32:03'),
    ('shoes', 'apparel-de', 0, '2020-12-24T06:32:03'),
    ('sunglasses', 'apparel-de', 1, '2021-01-01T06:32:03'),
    ('sunglas', 'apparel-de', 0, '2021-01-03T06:32:03'),
    ('sunglas', 'apparel-de', 0, '2021-01-09T06:32:03'),
    ('basecap', 'apparel-de', 0, '2021-01-12T06:32:03'),
    ('t-shirt', 'apparel-de', 0, '2021-01-21T06:32:03');
GO
